/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"

#include "ServerEntry.h"

#include <list>


/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"

    /**
     * The gateway function for soap_servers()
     * @param[in] fname the name of the file for the error messages
     * @return 0 if successful, a negative value otherwise
     */
    int sci_soap_servers(char *fname)
    {
        SciErr sciErr;

        // this function does not take input arguments
        CheckRhs(0, 0);

        // the number of output arguments must be between 1 and 3
        CheckLhs(1, 3);

        // get the server entries
        const std::list<ServerEntry> servers = SOAP::Instance().list();

        // allocate memory for values
        double* dOutIds = new double[servers.size()];
        char** cOutHostnames = new char*[servers.size()];
        double* dOutPorts = new double[servers.size()];

        // fill the arrays
        int i = 0;
        std::list<ServerEntry>::const_iterator it;
        for (it = servers.begin(); it != servers.end(); it++)
        {
            const ServerEntry& e = *it;
            dOutIds[i] = e.getId();
            cOutHostnames[i] = new char[e.getHostName().size() + 1];
            strcpy(cOutHostnames[i], e.getHostName().c_str());
            dOutPorts[i] = e.getPort();
            i++;
        }

        // create results on stack
        sciErr = createMatrixOfDouble(pvApiCtx, Rhs + 1, servers.size(), 1, dOutIds);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        sciErr = createMatrixOfString(pvApiCtx, Rhs + 2, servers.size(), 1, cOutHostnames);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        sciErr = createMatrixOfDouble(pvApiCtx, Rhs + 3, servers.size(), 1, dOutPorts);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        // free the arrays
        delete[] dOutIds;
        for (int i = 0; i < servers.size(); i++)
        {
            delete[] cOutHostnames[i];
        }
        delete[] cOutHostnames;
        delete[] dOutPorts;

        LhsVar(1) = Rhs + 1;
        LhsVar(2) = Rhs + 2;
        LhsVar(3) = Rhs + 3;

        // put the value on the stack
        PutLhsVar();

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
