/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "add.h"
#include "soap_argument_utilities.h"
#include "soap_error_codes.h"
#include "utilities.h"


/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"
    #include "MALLOC.h"
    #include "sci_types.h"
    #include "sciprint.h"

    #undef getType

    int sci_soap_add(char *fname)
    {
        SciErr sciErr;

        int m1               = 0;
        int n1               = 0;
        int* piVarOne        = NULL;
        int type1            = 0;

        int m2               = 0;
        int n2               = 0;
        int* piVarTwo        = NULL;
        int type2            = 0;

        char* hostname       = NULL;
        double val           = 0;
        int port             = 1090;

        // check that we have only 2 parameters input
        // check that we have only 1 parameters output
        CheckRhs(4, 4);
        CheckLhs(1, 1);

        // get the first argument (the host name)
        if (getSingleStringAtPosition(1, &hostname))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        // get the second argument (the port)
        if (getScalarDoubleAtPosition(2, &val))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        port = (int)val;
        // check the validity of the value
        if (port < 0 || port > 65535)
        {
            Scierror(9999, "%s: Invalid port number %d: Value in the range 0-65535 expected.\n", fname, port);
            return 0;
        }

        // get the third argument - the first summand
        sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piVarOne);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        sciErr = getVarType(pvApiCtx, piVarOne, &type1);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        // get the dimension
        sciErr = getVarDimension(pvApiCtx, piVarOne, &m1, &n1);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();
        }

        // get the fourth argument - the second summand
        sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piVarTwo);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        sciErr = getVarType(pvApiCtx, piVarTwo, &type2);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        // get the dimension
        sciErr = getVarDimension(pvApiCtx, piVarTwo, &m2, &n2);
        if (sciErr.iErr)
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();
        }

        // check that the variables are of the same type
        if (type1 != type2)
        {
            Scierror(999,"%s: Matrices must be of compatible types.\n", fname);
            return 0;
        }

        // check that the variables are of the same type
        if (type1 != sci_matrix && type1 != sci_sparse && type1 != sci_boolean && type1 != sci_boolean_sparse)
        {
            Scierror(999,"%s: Valid matrices types are double and boolean, including sparse.\n", fname);
            return 0;
        }

        // check size
        if ((m1 != m2) || (n1 != n2))
        {
            Scierror(999,"%s: Matrices must be of the same shape, (%d:%d) vs (%d:%d) found.\n", fname, m1, n1, m2, n2);
            return 0;
        }

        // the pointer to the returned object
        void * p = NULL;

        // call c function add
        if (add(hostname, port, piVarOne, piVarTwo, &p))
        {
            Scierror(REMOTE_CALL_ERROR, "%s: Remote invocation failed.\n", fname);
            return 0;
        }

        const int type = MatrixUtilities::getType(p);
        const int rows = MatrixUtilities::getRows(p);
        const int cols= MatrixUtilities::getCols(p);
        const int size = rows * cols;

        switch (type)
        {
            case sci_matrix:
            {
                const bool complex = DoubleMatrixUtilities::getComplex(p);

                // create a variable and copy data from the returned object
                if (complex)
                {
                    double * pRe = NULL;
                    double * pIm = NULL;

                    // allocate memory space for the resulting matrix
                    sciErr = allocComplexMatrixOfDouble(pvApiCtx, Rhs + 1, rows, cols, &pRe, &pIm);

                    double * pResRe = DoubleMatrixUtilities::getReDataPointer(p);
                    double * pResIm = DoubleMatrixUtilities::getImDataPointer(p);

                    for (int i = 0; i < size; i++)
                    {
                        pRe[i] = pResRe[i];
                        pIm[i] = pResIm[i];
                    }
                }
                else
                {
                    double * pRe = NULL;

                    // allocate memory space for the resulting matrix
                    sciErr = allocMatrixOfDouble(pvApiCtx, Rhs + 1, rows, cols, &pRe);

                    double * pResRe = DoubleMatrixUtilities::getReDataPointer(p);

                    for (int i = 0; i < size; i++)
                    {
                        pRe[i] = pResRe[i];
                    }
                }
                break;
            }
            case sci_boolean:
            {
                int * piRes = BooleanMatrixUtilities::getDataPointer(p);

                // allocate memory space for the resulting matrix
                sciErr = createMatrixOfBoolean(pvApiCtx, Rhs + 1, rows, cols, piRes);
                break;
            }
            case sci_boolean_sparse:
            {
                const int total = SparseMatrixUtilities::getTotal(p);
                int * lines = SparseMatrixUtilities::getLineItems(p);
                int * columnPositions = SparseMatrixUtilities::getColumnPositions(p);

                // allocate memory space for the resulting matrix
                sciErr = createBooleanSparseMatrix(pvApiCtx, Rhs + 1, rows, cols, total, lines, columnPositions);
                break;
            }
            case sci_sparse:
            {
                const bool complex = DoubleMatrixUtilities::getComplex(p);

                // create a variable and copy data from the returned object
                if (complex)
                {
                    const int total = SparseMatrixUtilities::getTotal(p);
                    int* lines = SparseMatrixUtilities::getLineItems(p);
                    int* columnPositions = SparseMatrixUtilities::getColumnPositions(p);
                    double* pdRe = DoubleSparseMatrixUtilities::getReDataPointer(p);
                    double* pdIm = DoubleSparseMatrixUtilities::getImDataPointer(p);

                    // allocate memory space for the resulting matrix
                    sciErr = createComplexSparseMatrix(pvApiCtx, Rhs + 1, rows, cols, total,
                        lines, columnPositions, pdRe, pdIm);
                }
                else
                {
                    const int total = SparseMatrixUtilities::getTotal(p);
                    int* lines = SparseMatrixUtilities::getLineItems(p);
                    int* columnPositions = SparseMatrixUtilities::getColumnPositions(p);
                    double* pdRe = DoubleSparseMatrixUtilities::getReDataPointer(p);

                    // allocate memory space for the resulting matrix
                    sciErr = createSparseMatrix(pvApiCtx, Rhs + 1, rows, cols, total,
                        lines, columnPositions, pdRe);
                }
                break;
            }
            default:
            {
                Scierror(12345, "%s: Object of wrong type %d returned by the remote method.\n", fname, type);
                break;
            }
        }

        if (p != NULL)
        {
            free(p);
        }

        // release the memory for the allocated string (the host name)
        if (hostname != NULL)
        {
            freeAllocatedSingleString(hostname);
        }

        LhsVar(1) = Rhs + 1;

        // This function put on scilab stack, the lhs variable
        // which are at the position lhs(i) on calling stack
        // You need to add PutLhsVar here because WITHOUT_ADD_PUTLHSVAR
        // was defined and equal to %t
        // without this, you do not need to add PutLhsVar here
        PutLhsVar();

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
