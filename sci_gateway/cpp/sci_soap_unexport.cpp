/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"
#include "../../src/cpp/soap_server_exceptions.h"
#include "soap_argument_utilities.h"
#include "soap_scilab_utilities.h"
#include "soap_error_codes.h"

#include <list>
#include <algorithm>

/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"

    static int readArgs(char *fname, char*** functions, int* functionRows, int* functionCols, int* servers);

    /**
     * The gateway function for soap_export()
     * @param[in] fname the name of the file for the error messages
     * @return 0 if successful, a negative value otherwise
     */
    int sci_soap_unexport(char *fname)
    {
        char** functions       = NULL;
        int functionsRows      = 0;
        int functionsCols      = 0;

        int server             = -1;

        const int minRhsArgs   = 1;
        const int maxRhsArgs   = 2;

        // the number of input parameters must be between minRhsArgs and maxRhsArgs
        CheckRhs(minRhsArgs, maxRhsArgs);

        // this function does not return values
        // CheckLhs(0, 1);

        // read rhs arguments
        if (readArgs(fname, &functions, &functionsRows, &functionsCols, &server))
        {
            return 0;
        }

        // fill the list
        std::list<std::string> functionList;
        for (int i = 0; i < functionsRows * functionsCols; i++)
        {
            // check that there is no such function in the list yet
            if (std::find(functionList.begin(), functionList.end(), functions[i]) == functionList.end())
            {
                functionList.push_back(functions[i]);
            }
            else
            {
                Scierror(MULTIPLE_ENTRIES_ERROR,
                    "%s: Multiple entries of function %s in the list of functions.\n", fname, functions[i]);

                // free the allocated memory
                if (functions != NULL)
                {
                    freeAllocatedMatrixOfString(functionsRows, functionsCols, functions);
                }
                return 0;
            }
        }

        try
        {
            if (Rhs > 1)
            {
                SOAP::Instance().unexportFunctions(functionList, server);
            }
            else
            {
                SOAP::Instance().unexportFunctions(functionList);
            }
        }
        catch (NoServerRunningException)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: No server is running.\n", fname);
        }
        catch (ServerNotFoundException e)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: Server with ID %d not found.\n", fname, e.getServerId());
        }
        catch (MultipleServersRunningException)
        {
            Scierror(MULTIPLE_SERVERS_ERROR, "%s: %s\n%s\n", fname,
                "Multiple SOAP servers are running:",
                "You must choose one of the servers using the second argument.");
        }
        catch (FunctionNotExportedException e)
        {
            Scierror(EXPORT_FAILURE, "%s: %s has not been exported.\n", fname, e.getFunction().c_str());
        }

        // free the allocated memory
        if (functions != NULL)
        {
            freeAllocatedMatrixOfString(functionsRows, functionsCols, functions);
        }

        // no return variable
        LhsVar(1) = 0;

        // put the value on the stack
        PutLhsVar();

        return 0;
    }

    /**
     * Reads and checks the validity of right-hand side arguments of the function
     * @param[in] fname the name of the file passed from the gateway function
     * @param[out] ip_addr the value of the IP address argument
     * @param[out] port the value of the port argument
     * @return 0 if successful, a negative value otherwise
     */
    int readArgs(char *fname, char*** functions, int* functionsRows, int* functionsCols, int* server)
    {
        // get the first argument - functions
        if (getStringMatrixAtPosition(1, functionsRows, functionsCols, functions))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return -1;
        }

        if (Rhs > 1)
        {
            double value;

            // get the first argument - servers
            if (getScalarDoubleAtPosition(2, &value))
            {
                // no return variable
                LhsVar(1) = 0;

                // put the value on the stack
                PutLhsVar();

                return -1;
            }

            *server = (int)value;
        }

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
