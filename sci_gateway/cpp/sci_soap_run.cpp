/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"
#include "../../src/cpp/soap_server_exceptions.h"
#include "soap_argument_utilities.h"
#include "soap_error_codes.h"

/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"
    #include "MALLOC.h"
    #include "sciprint.h"
    #include "warningmode.h"

    static int checkPortValue(char *fname, int port);

    static int readArgs(char *fname, char** hostname, int* port);

    /**
     * The gateway function for soap_run()
     * @param[in] fname the name of the file for the error messages
     * @return 0 if successful, a negative value otherwise
     */
    int sci_soap_run(char *fname)
    {
        double dOut           = 0.0;

        // default values
        char* hostname        = NULL;
        int port              = 1080;

        int minRhsArgs        = 0;
        int maxRhsArgs        = 2;

        // the number of input parameters must be between minRhsArgs and maxRhsArgs
        CheckRhs(minRhsArgs, maxRhsArgs);

        // the number of output arguments must be 1
        CheckLhs(1, 1);

        // read rhs arguments
        if (readArgs(fname, &hostname, &port)) return 0;

        int serverId;
        // call the c function
        try
        {
            if (hostname == NULL)
            {
                serverId = SOAP::Instance().run("127.0.0.1", port);
            }
            else
            {
                serverId = SOAP::Instance().run(hostname, port);

                // free the allocated memory
                freeAllocatedSingleString(hostname);
            }
        }
        catch (ServerStartException)
        {
            if (hostname != NULL)
            {
                Scierror(SOAP_SERVER_STARTUP_FAILURE, 
                    "%s: Starting SOAP server on %s:%d failed.\n", fname, hostname, port);
            }
            else
            {
                Scierror(SOAP_SERVER_STARTUP_FAILURE, 
                    "%s: Starting SOAP server on <default hostname>:%d failed.\n", fname, port);
            }
            return 0;
        }

        // put the id of the server in the variable to be output
        dOut = serverId;

        // create result on stack
        createScalarDouble(pvApiCtx, Rhs + 1, dOut);

        LhsVar(1) = Rhs + 1;

        // Put the value on the stack
        PutLhsVar();

        return 0;
    }

    /**
     * Reads and check the validity of right-hand side arguments of the function
     * @param[in] fname the name of the file passed from the gateway function
     * @param[out] hostname the pointer to the hostname argument
     * @param[out] port the pointer to the port argument
     * @return 0 if successful, a negative value otherwise
     */
    int readArgs(char *fname, char** hostname, int* port)
    {
        double temp            = 0;
        int temp_int           = 0;

        int useDefaultPort     = 0;
        int useDefaultHostname = 0;

        switch (Rhs)
        {
            case 0:
            {
                // no arguments given - keep the default values
                useDefaultPort = 1;
                useDefaultHostname = 1;
                break;
            }

            case 1:
            {
                // one argument specified - it must be the hostname
                int ret = checkAndGetSingleString(fname, 1, hostname);
                if (ret < 0)
                {
                    switch (ret)
                    {
                        case -1:
                            // the error message was already printed in checkAndGetSingleString()
                            break;
                        case -2:
                            Scierror(INVALID_ARGUMENT_TYPE,
                                "%s: Wrong type of input argument #%d: A string value expected.\n", fname, 1);
                            break;
                        case -3:
                            Scierror(INVALID_ARGUMENT_SIZE,
                                "%s: Wrong size of input argument #%d: A single string expected.\n", fname, 1);
                            break;
                        default:
                            Scierror(ERROR_READING_ARGUMENT, "%s: Error reading input argument #%d.\n", fname, 1);
                            break;
                    }
                    return -1;
                }
                else if (ret == 1)
                {
                    // empty matrix supplied - print error message
                    Scierror(INVALID_ARGUMENT_SIZE, "%s: Wrong size of input argument #%d: A single string expected.\n",
                        fname, 1);
                    return -1;
                }
                else
                {
                    useDefaultPort = 1;
                }
                break;
            }

            case 2:
            {
                // two parameter supplied
                // get the hostname value
                int ret1 = checkAndGetSingleString(fname, 1, hostname);
                if (ret1 < 0)
                {
                    switch (ret1)
                    {
                        case -1:
                            // the error message was already printed in checkAndGetSingleString()
                            break;
                        case -2:
                            Scierror(INVALID_ARGUMENT_TYPE,
                                "%s: Wrong type of input argument #%d: A string value expected.\n", fname, 1);
                            break;
                        case -3:
                            Scierror(INVALID_ARGUMENT_SIZE,
                                "%s: Wrong size of input argument #%d: A single string or empty matrix expected.\n",
                                fname, 1);
                            break;
                        default:
                            Scierror(ERROR_READING_ARGUMENT, "%s: Error reading input argument #%d.\n", fname, 1);
                            break;
                    }
                    return -1;
                }
                useDefaultHostname = ret1;

                // get the port value
                if (getScalarDoubleAtPosition(2, &temp))
                {
                    // no return variable
                    LhsVar(1) = 0;

                    // put the value on the stack
                    PutLhsVar();

                    return -3;
                }
                else
                {
                    temp_int = (int)temp;

                    // check the port value for validity
                    if (checkPortValue(fname, temp_int))
                    {
                        return -1;
                    }

                    *port = temp_int;
                }
                break;
            }

            default:
            {
                // this block should never be reached
                return -1;
                // break;
            }
        }

        // print warnings regarding default values
        if (getWarningMode())
        {
            if (useDefaultHostname == 1)
            {
                sciprint("WARNING: using the default host name\n");
            }

            if (useDefaultPort == 1)
            {
                sciprint("WARNING: using the default port number\n");
            }
        }

        return 0;
    }

    /**
     * Checks that the port number lies withing the valid range
     * @param[in] fname the name of the file passed from the gateway function
     * @param[in] port the port number to be validated
     * @return 0 if the port number is valid, a negative value otherwise
     */
    int checkPortValue(char *fname, int port)
    {
        // check that the value is in the valid range
        if (port < 0 || port > 65535)
        {
            Scierror(INVALID_PORT_NUMBER, "%s: Invalid port number %d: Value in the range 0-65535 expected.\n",
                fname, port);
            return -1;
        }

        // print warning if the value is a well-known port number (0-1023)
        if (port < 1024)
        {
            if (getWarningMode())
            {
                sciprint("WARNING :\n");
                sciprint("%s: Port number %d: The use of a privileged port number (0-1023) is discouraged.\n",
                    fname, port);
            }
        }

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
