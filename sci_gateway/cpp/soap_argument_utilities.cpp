/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"

#include "soap_argument_utilities.h"

int getScalarDoubleAtPosition(int position, double* val)
{
    SciErr sciErr;

    //variable info
    int *piAddr           = NULL;

    // get the address of the parameter variable
    sciErr = getVarAddressFromPosition(pvApiCtx, position, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return -1;
    }

    return getScalarDouble(pvApiCtx, piAddr, val);
}

int getSingleStringAtPosition(int position, char** pstr)
{
    SciErr sciErr;

    //variable info
    int *piAddr           = NULL;

    // get the address of the parameter variable
    sciErr = getVarAddressFromPosition(pvApiCtx, position, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return -1;
    }

    return getAllocatedSingleString(pvApiCtx, piAddr, pstr);
}

int getDoubleMatrixAtPosition(int position, int* rows, int* cols, double** values)
{
    SciErr sciErr;

    //variable info
    int *piAddr           = NULL;

    // get address of inputs
    sciErr = getVarAddressFromPosition(pvApiCtx, position, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getMatrixOfDouble(pvApiCtx, piAddr, rows, cols, values);
    if (sciErr.iErr)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

int getStringMatrixAtPosition(int position, int* rows, int* cols, char*** strings)
{
    SciErr sciErr;

    //variable info
    int *piAddr           = NULL;

    // get address of inputs
    sciErr = getVarAddressFromPosition(pvApiCtx, position, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    return getAllocatedMatrixOfString(pvApiCtx, piAddr, rows, cols, strings);
}

int checkAndGetSingleString(char *fname, int position, char** str)
{
    SciErr sciErr;

    //variable info
    int* piAddr           = NULL;
    int rows              = 0;
    int cols              = 0;
    char** pstData        = NULL;
    char* buf             = NULL;
    char** temp           = &buf;

    // get the address of the parameter variable
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return -1;
    }

    // check the dimension
    sciErr = getVarDimension(pvApiCtx, piAddr, &rows, &cols);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return -1;
    }

    if (rows == 0 && cols == 0)
    {
        // empty matrix is valid - keep the default value
        return 1;
    }
    else if (rows == 1 && cols == 1)
    {
        // a scalar is valid - proceed
    }
    else
    {
        // other shapes are invalid
        return -3;
    }

    // check input type
    if (!isStringType(pvApiCtx, piAddr))
    {
        return -2;
    }

    // retrieve the string
    if (getAllocatedSingleString(pvApiCtx, piAddr, temp))
    {
        // NOTE: this block should never be reached
        // no return variable
        LhsVar(1) = 0;

        // put the value on the stack
        PutLhsVar();

        return -1;
    }

    // set the return value
    *str = buf;

    return 0;
}
