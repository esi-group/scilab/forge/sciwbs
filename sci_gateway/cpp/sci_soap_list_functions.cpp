/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"
#include "soap_argument_utilities.h"
#include "soap_error_codes.h"
#include "list_functions_client.h"

#include <list>
#include <string>

/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"

    static int getListLocal(char* fname);
    static int getListRemote(char* fname);

    static int outputList(std::list<std::string>& functions);

    /**
     * The gateway function for soap_list_functions()
     * @param[in] fname the name of the file for the error messages
     * @return 0 if successful, a negative value otherwise
     */
    int sci_soap_list_functions(char *fname)
    {
        // this function takes 0 to 2 arguments
        CheckRhs(0, 2);

        // the number of output arguments must be 1
        CheckLhs(1, 1);

        if (Rhs == 2)
        {
            getListRemote(fname);
        }
        else
        {
            getListLocal(fname);
        }

        return 0;
    }

    int getListLocal(char* fname)
    {
        SciErr sciErr;

        int id;

        // if the server ID is supplied
        if (Rhs > 0)
        {
            double val = 0;

            // read the argument - the id of the server to be terminated
            if (getScalarDoubleAtPosition(1, &val))
            {
                // no return variable
                LhsVar(1) = 0;

                // put the value on the stack
                PutLhsVar();

                return 0;
            }

            id = (int)val;
        }

        std::list<std::string> functions;

        try
        {
            if (Rhs > 0)
            {
                functions = SOAP::Instance().listFunctions(id);
            }
            else
            {
                functions = SOAP::Instance().listFunctions();
            }
        }
        catch (NoServerRunningException)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: No server is running.\n", fname);
            return 0;
        }
        catch (ServerNotFoundException e)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: Server with ID %d not found.\n", fname, e.getServerId());
            return 0;
        }
        catch (MultipleServersRunningException)
        {
            Scierror(MULTIPLE_SERVERS_ERROR, "%s: %s\n%s\n", fname,
                "Multiple SOAP servers are running:", "You must choose one of the servers.");
            return 0;
        }

        outputList(functions);

        LhsVar(1) = Rhs + 1;

        // put the value on the stack
        PutLhsVar();

        return 0;
    }

    int getListRemote(char* fname)
    {
        SciErr sciErr;

        char* hostname = NULL;
        int port;

        if (getSingleStringAtPosition(1, &hostname))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        double value = 0;

        if (getScalarDoubleAtPosition(2, &value))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        port = (int)value;

        try
        {
            std::list<std::string> functions = retrieveFunctionList(hostname, port);

            outputList(functions);

            LhsVar(1) = Rhs + 1;

            // put the value on the stack
            PutLhsVar();
        }
        catch (RemoteCallException e)
        {
            Scierror(REMOTE_CALL_ERROR, "%s: Remote invocation failed.\n", fname);
            return 0;
        }

        return 0;
    }

    int outputList(std::list<std::string>& functions)
    {
        SciErr sciErr;

        // allocate memory for values
        char** cOutFunctions = new char*[functions.size()];

        // fill the array
        int i = 0;
        std::list<std::string>::const_iterator it;
        for (it = functions.begin(); it != functions.end(); it++)
        {
            cOutFunctions[i] = new char[it->size() + 1];
            strcpy(cOutFunctions[i], it->c_str());
            i++;
        }

        // create result on stack
        sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, functions.size(), 1, cOutFunctions);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        // free the array
        for (int i = 0; i < functions.size(); i++)
        {
            delete[] cOutFunctions[i];
        }
        delete[] cOutFunctions;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
