/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "soap_scilab_utilities.h"

#include "call_scilab.h"
#include "api_common.h"
#include "api_internal_common.h"
#include "stack-c.h"
#include "stackinfo.h"
#include "Scierror.h"

extern "C"
{
    #include "Funtab.h"
    #include "stack-def.h"
    #include "searchmacroinlibraries.h"
}

static int getNamedVariableAddress(void* _pvCtx, char* _pstName, int** _piAddress);

bool isScilabPrimitive(char* name)
{
    // some magic here
    int id[nsiz];
    int funptr = 0;
    int zero = 0;
    int job = 1;   // Find function & returns fptr value

    C2F(cvname)(id, name, &zero, (unsigned long)strlen(name));
    C2F(funtab)(id, &funptr, &job, "NULL_NAME", 0);

    return funptr != 0;
}

bool isScilabLibraryMacro(char* name)
{
    int n = 0;
    char** result = searchmacroinlibraries(name, &n);
    if (result != NULL)
    {
        // TODO check if it is necessary to free the memory for each string
        delete[] result;
        return true;
    }
    else
    {
        return false;
    }
}

bool isScilabUserMacro(char* name)
{
    int *piAddr           = NULL;
    int type              = 0;

    // get the address of the named variable
    if (getNamedVariableAddress(pvApiCtx, name, &piAddr))
    {
        return false;
    }

    // get the type of the variable
    SciErr sciErr = getVarType(pvApiCtx, piAddr, &type);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return -1;
    }

    return type == sci_c_function || type == sci_u_function;
}

int getNamedVariableAddress(void* _pvCtx, char* _pstName, int** _piAddress)
{
    SciErr sciErr;
    sciErr.iErr = 0;
    sciErr.iMsgCount = 0;

    int iVarID[nsiz];
    int* piAddr	= NULL;

    //get variable id from name
    C2F(str2name)(_pstName, iVarID, (int)strlen(_pstName));

    //define scope of search
    Fin = -1;
    Err = 0;

    //search variable
    C2F(stackg)(iVarID);

    //No idea :(
    if (*Infstk(Fin) == 2)
    {
        Fin = *istk(iadr(*Lstk(Fin )) + 1 + 1);
    }

    if (Err > 0 || Fin == 0)
    {
        return -1;
    }

    //get variable address
    getNewVarAddressFromPosition(_pvCtx, Fin, &piAddr);

    *_piAddress = piAddr;
    return 0;
}
