<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="soap_run" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>soap_run</refname>

    <refpurpose>runs a SOAP server on given host name and port</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>id = soap_run()</synopsis>
    <synopsis>id = soap_run(hostname)</synopsis>
    <synopsis>id = soap_run(hostname, port)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>hostname</term>

        <listitem>
          <para>a single string representing the host name to run the server on</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>port</term>

        <listitem>
          <para>a scalar double value representing the port to run the server on</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>id</term>

        <listitem>
          <para>the id of the started server</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Starts a SOAP server on given host name and port. The function will wait until either the server has
    started successfully or an error value has been returned. In the latter case, a corresponding error message
    will be printed.</para>
    <para>Once the server has started, it is immediately available for requests.</para>
    <para>The returned value is the identifier of the started server that should be used for further
    operations on the server, such as soap_terminate, soap_export or soap_unexport.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <para>To run the SOAP server with the default host name and port values, execute the following command:</para>
    <programlisting role="example">soap_run()</programlisting>

    <para>To specify a different host name (for example, 'foobar.com'), type:</para>
    <programlisting role="example">soap_run('foobar.com')</programlisting>
    <para>The default port number will be used.</para>

    <para>If you want to use a different port number, but keep the default host name ('127.0.0.1'), use</para>
    <programlisting role="example">soap_run([], 1080)</programlisting>
    <para>This will start the server on port 1080 (if it is not already bound) on the local host.</para>

    <para>To specify both host name and port, use the complete syntax:</para>
    <programlisting role="example">soap_run('foobar.com', 1234)</programlisting>
    <para>This command will start a SOAP server listening to port 1234 on host 'foobar.com'.</para>
  </refsection>

  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="soap_terminate">soap_terminate</link>
      </member>
      <member>
        <link linkend="soap_servers">soap_servers</link>
      </member>
      <member>
        <link linkend="soap_export">soap_export</link>
      </member>
      <member>
        <link linkend="soap_unexport">soap_unexport</link>
      </member>
      <member>
        <link linkend="soap_list_functions">soap_list_functions</link>
      </member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Artem GLEBOV</member>
    </simplelist>
  </refsection>
</refentry>
