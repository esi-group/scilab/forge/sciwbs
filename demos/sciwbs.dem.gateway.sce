// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - INRIA - Artem GLEBOV
//
// This file is released into the public domain

demopath = get_absolute_file_path("sciwbs.dem.gateway.sce");

subdemolist = ["demo soap_add"               ,"soap_add.dem.sce"; ];

subdemolist(:,2) = demopath + subdemolist(:,2);
