// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - INRIA - Artem GLEBOV
//
// This file is released into the public domain

mode(-1);
lines(0);

disp("soap_run([], 12345)");
disp(soap_run([], 12345));
disp("soap_add(''localhost'', 12345, [1 2 3], [7 6 5])");
disp(soap_add('localhost', 12345, [1 2 3], [7 6 5]));
