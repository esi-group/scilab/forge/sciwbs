/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __INVOKE_H__
#define __INVOKE_H__

#include <libxml/tree.h>

/**
 * Performs a remote invocation of the specified web-service
 * @param[in] endpoint the endpoint of the Web-service to be called
 * @param[in] action the action to be invoked
 * @param[in] request the request string
 * @param[out] response the response string
 * @return 0 if successful, a negative value otherwise
 */
int invoke(const char* const endpoint, const char* const action, xmlDoc* const request, xmlDoc** const response);

#endif /* __INVOKE_H__ */

