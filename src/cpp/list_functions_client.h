/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __LIST_FUNCTIONS_CLIENT_H__
#define __LIST_FUNCTIONS_CLIENT_H__

#include <list>
#include <string>

/**
 * Invokes a remote SOAP method to retrieve the list of functions
 * @param[in] hostname the host name of the SOAP server to use
 * @param[in] port the port of the SOAP server to use
 * @return 0 if successful, a negative value otherwise
 */
std::list<std::string> retrieveFunctionList(const char* const hostname, const int port);

#endif /* __LIST_FUNCTIONS_CLIENT_H__ */

