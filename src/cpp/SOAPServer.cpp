/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAPServer.h"

#include "gsoap/soapH.h"
//#include "gsoap/nsmap.h"

#include <algorithm>

// A wrapper function for thread creation with option of passing parameter
#ifdef _MSC_VER
#define __CreateThreadParam(threadId, functionName, param)  *(threadId) = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)functionName, param, 0, NULL)
#else
#define __CreateThreadParam(threadId, functionName, param)  pthread_create(threadId, NULL, functionName, param)
#endif //_MSC_VER

void* server_routine(void* arg);

SOAPServer::SOAPServer()
{
    // initialize the general object lock
    __InitLock(&objectLock);

    state = 0;

    // initialize the condition and lock for the returnValue variable
    __InitLock(&serverStartLock);
    __InitSignal(&serverStartSignal);

    returnValue = 0;
}

SOAPServer::~SOAPServer()
{
}

void SOAPServer::run(const char* const h, const int p)
{
    // lock the object
    __Lock(&objectLock);

    // check the server is not running
    if (state != 0)
    {
        __UnLock(&objectLock);
        throw IllegalStateException();
    }

    // store the parameters
    hostname = h;
    port = p;

    // start the new thread
    if (__CreateThreadParam(&threadId, server_routine, this))
    {
        __UnLock(&objectLock);
        throw ServerStartException();
    }

    // wait for the server to start
    __Lock(&serverStartLock);
    if (state == 0)
    {
        __Wait(&serverStartSignal, &serverStartLock);

        if (returnValue != 0)
        {
            __UnLock(&objectLock);
            throw ServerStartException();
        }
    }
    __UnLock(&serverStartLock);

    // unlock the object
    __UnLock(&objectLock);
}

void SOAPServer::terminate()
{
    // lock the object
    __Lock(&objectLock);

    if (state != 1)
    {
        __UnLock(&objectLock);
        throw IllegalStateException();
    }
    if (__Terminate(threadId))
    {
        __UnLock(&objectLock);
        throw ServerTerminationException();
    }

    state = 0;

    // unlock the object
    __UnLock(&objectLock);
}

void SOAPServer::exportFunction(std::string& functionName)
{
    if (std::find(functions.begin(), functions.end(), functionName) != functions.end())
    {
        throw FunctionAlreadyExportedException(functionName);
    }

    functions.push_back(functionName);
}

void SOAPServer::exportFunction(char* functionName)
{
    std::string str(functionName);
    exportFunction(str);
}

void SOAPServer::unexportFunction(std::string& functionName)
{
    std::list<std::string>::iterator it =
        std::find(functions.begin(), functions.end(), functionName);
    if (it == functions.end())
    {
        // this function has not been exported on this server
        throw FunctionNotExportedException(functionName);
    }

    // function found - remove it from the list
    functions.erase(it);
}

void SOAPServer::unexportFunction(char* functionName)
{
    std::string str(functionName);
    unexportFunction(str);
}

void* server_routine(void* arg)
{
    SOAPServer* server = (SOAPServer*)arg;

    // lock the result mutex
    __Lock(&server->serverStartLock);

    SOAP_SOCKET m, s;  // master and slave sockets
    struct soap soap;
    soap_init(&soap);
    m = soap_bind(&soap, server->getHostName().c_str(), server->getPort(), 100);

    if (!soap_valid_socket(m))
    {
        soap_print_fault(&soap, stderr);

        // set the state to terminated, the return value to failure
        server->state = 0;
        server->returnValue = 1;

        // signal to the waiting method
        __Signal(&server->serverStartSignal);

        // unlock the result mutex
        __UnLock(&server->serverStartLock);

        return NULL;
    }

    // set the state to running, the return value to success
    server->state = 1;
    server->returnValue = 0;

    // signal to the waiting method
    __Signal(&server->serverStartSignal);

    // unlock the result mutex
    __UnLock(&server->serverStartLock);

    for ( ; ; )
    {
        s = soap_accept(&soap);

        if (!soap_valid_socket(s))
        {
            soap_print_fault(&soap, stderr);
            return (void*)-1;
        }
        soap_serve(&soap);
        if (soap.error)
        {
            soap_print_fault(&soap, stderr);
        }
        soap_end(&soap);
    }

    return NULL;
}
