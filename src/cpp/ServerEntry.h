/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SERVER_ENTRY_H__
#define __SERVER_ENTRY_H__

#include <string>

class ServerEntry
{
private:
    int id;
    std::string hostname;
    int port;

public:
    ServerEntry(const int _id, const std::string& _hostname, const int _port)
    {
        id = _id;
        hostname = _hostname;
        port = _port;
    }

    ServerEntry(const int _id, const char* const _hostname, const int _port)
    {
        id = _id;
        hostname = _hostname;
        port = _port;
    }

    ~ServerEntry() {}

    int getId()                      const { return id;        }
    const std::string& getHostName() const { return hostname;  }
    int getPort()                    const { return port;      }
};

#endif /* __SERVER_ENTRY_H__ */
