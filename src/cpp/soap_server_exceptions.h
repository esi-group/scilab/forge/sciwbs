/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_SERVER_EXCEPTIONS_H__
#define __SOAP_SERVER_EXCEPTIONS_H__

#include <string>

class ServerStartException {};
class ServerTerminationException {};

class IllegalStateException {};

class FunctionException
{
private:
    std::string function;
public:
    FunctionException(std::string& f) { function = f; }
    const std::string& getFunction() const { return function; }
};

class FunctionAlreadyExportedException : public FunctionException
{
public:
    FunctionAlreadyExportedException(std::string& f) : FunctionException(f) {}
};

class FunctionNotExportedException : public FunctionException
{
public:
    FunctionNotExportedException(std::string& f) : FunctionException(f) {}
};

#endif /* __SOAP_SERVER_EXCEPTIONS_H__ */
