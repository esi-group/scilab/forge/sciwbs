/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __UTILITIES_H__
#define __UTILITIES_H__

/**
 * Formats the host name and port into an HTTP adrress string
 * @param[in] hostname the host name to be used
 * @param[in] port the port to be used
 * @return the pointer to the char array containing the resulting string
 * (must be released after use)
 */
char* getHttpAddress(const char* const hostname, const int port);

/**
 * Utility class containing function for extracting values from a Scilab internal matrix representation
 */
class MatrixUtilities
{
public:
    inline static int* getIntPointer(void* pointer) { return reinterpret_cast<int*>(pointer); }
    inline static const int* getIntPointerConst(const void* pointer) { return reinterpret_cast<const int*>(pointer); }

    inline static int getType(const void* pointer) { return getIntPointerConst(pointer)[0]; }
    inline static void setType(void* pointer, int type) { getIntPointer(pointer)[0] = type; }

    inline static int getRows(const void* pointer) { return getIntPointerConst(pointer)[1]; }
    inline static void setRows(void* pointer, int rows) { getIntPointer(pointer)[1] = rows; }

    inline static int getCols(const void* pointer) { return getIntPointerConst(pointer)[2]; }
    inline static void setCols(void* pointer, int cols) { getIntPointer(pointer)[2] = cols; }
};

class DoubleMatrixUtilities
{
public:
    inline static int getComplex(const void* pointer)
        { return MatrixUtilities::getIntPointerConst(pointer)[3]; }
    inline static void setComplex(void* pointer, int complex)
        { MatrixUtilities::getIntPointer(pointer)[3] = complex; }

    inline static double* getReDataPointer(void* pointer)
        { return reinterpret_cast<double*>(&MatrixUtilities::getIntPointer(pointer)[4]); }
    inline static const double* getReDataPointerConst(const void* pointer)
        { return reinterpret_cast<const double*>(&MatrixUtilities::getIntPointerConst(pointer)[4]); }

    inline static double* getImDataPointer(void* pointer)
        { return reinterpret_cast<double*>(&MatrixUtilities::getIntPointer(pointer)[4]) +
              MatrixUtilities::getRows(pointer) * MatrixUtilities::getCols(pointer); }
    inline static const double* getImDataPointerConst(const void* pointer)
        { return reinterpret_cast<const double*>(&MatrixUtilities::getIntPointerConst(pointer)[4]) +
              MatrixUtilities::getRows(pointer) * MatrixUtilities::getCols(pointer); }

    inline static int getStorageSize(int rows, int cols, bool complex)
        { return sizeof(int) * 4 + sizeof(double) * rows * cols * (complex ? 2 : 1); }
};

class BooleanMatrixUtilities
{
public:
    inline static int* getDataPointer(void* pointer)
        { return &MatrixUtilities::getIntPointer(pointer)[3]; }
    inline static const int* getDataPointerConst(const void* pointer)
        { return &MatrixUtilities::getIntPointerConst(pointer)[3]; }

    inline static int getStorageSize(int rows, int cols)
        { return sizeof(int) * (3 + rows * cols); }
};

class SparseMatrixUtilities
{
public:
    inline static int getTotal(const void* pointer)
        { return MatrixUtilities::getIntPointerConst(pointer)[4]; }
    inline static void setTotal(void* pointer, int total)
        { MatrixUtilities::getIntPointer(pointer)[4] = total; }

    inline static int* getLineItems(void* pointer)
        { return &MatrixUtilities::getIntPointer(pointer)[5]; }
    inline static const int* getLineItemsConst(const void* pointer)
        { return &MatrixUtilities::getIntPointerConst(pointer)[5]; }

    inline static int* getColumnPositions(void* pointer)
        { return &MatrixUtilities::getIntPointer(pointer)[5 + MatrixUtilities::getRows(pointer)]; }
    inline static const int* getColumnPositionsConst(const void* pointer)
        { return &MatrixUtilities::getIntPointerConst(pointer)[5 + MatrixUtilities::getRows(pointer)]; }

    /**
     * Checks if the element at a given position is non-zero (i.e. present in this sparse matrix)
     * @param[in] pointer the pointer to the Scilab structure representing a sparse boolean matrix
     * @param[in] row null-based row index of the element to be checked
     * @param[in] col null-based column index of the element to be checked
     * @return true if the element is non-zero (i.e. present in this sparse matrix), false otherwise
     */
    inline static bool isElementPresent(void* pointer, int row, int col)
        {
            const int* lines = getLineItemsConst(pointer);
            const int* columnPositions = getColumnPositionsConst(pointer);
            int previousLinesElements = 0;
            for (int i = 0; i < row; i++)
            {
                previousLinesElements += lines[i];
            }
            int thisLineElements = lines[row];
            for (int i = 0; i < thisLineElements; i++)
            {
                 if (columnPositions[previousLinesElements + i] == col + 1)
                     return true;
            }
            return false;
        }
};

class BooleanSparseMatrixUtilities
{
public:
    inline static int getStorageSize(int rows, int total)
        { return sizeof(int) * (5 + rows + total); }
};

class DoubleSparseMatrixUtilities
{
public:
    inline static int getComplex(const void* pointer)
        { return MatrixUtilities::getIntPointerConst(pointer)[3]; }
    inline static void setComplex(void* pointer, int complex)
        { MatrixUtilities::getIntPointer(pointer)[3] = complex; }

    inline static double* getReDataPointer(void* pointer)
        { return reinterpret_cast<double*>(&MatrixUtilities::getIntPointer(pointer)[6 + 
              MatrixUtilities::getRows(pointer) + SparseMatrixUtilities::getTotal(pointer)]); }
    inline static const double* getReDataPointerConst(const void* pointer)
        { return reinterpret_cast<const double*>(&MatrixUtilities::getIntPointerConst(pointer)[6 + 
              MatrixUtilities::getRows(pointer) + SparseMatrixUtilities::getTotal(pointer)]); }

    inline static double* getImDataPointer(void* pointer)
        { return reinterpret_cast<double*>(&MatrixUtilities::getIntPointer(pointer)[6 + 
              MatrixUtilities::getRows(pointer) + SparseMatrixUtilities::getTotal(pointer)]) +
                  SparseMatrixUtilities::getTotal(pointer); }
    inline static const double* getImDataPointerConst(const void* pointer)
        { return reinterpret_cast<const double*>(&MatrixUtilities::getIntPointerConst(pointer)[6 + 
              MatrixUtilities::getRows(pointer) + SparseMatrixUtilities::getTotal(pointer)]) +
                  SparseMatrixUtilities::getTotal(pointer); }

    inline static int getStorageSize(int rows, int total, bool complex)
        { return sizeof(int) * (5 + rows + total) + sizeof(double) * total * (complex ? 2 : 1); }
};

#endif /* __UTILITIES_H__ */

