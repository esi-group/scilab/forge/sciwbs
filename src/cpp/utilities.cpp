/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "utilities.h"

#include <stdio.h>
#include <string.h>

/**
 * Formats the host name and port into an HTTP adrress string
 * @param[in] hostname the host name to be used
 * @param[in] port the port to be used
 * @return the pointer to the char array containing the resulting string
 * (must be released after use)
 */
char* getHttpAddress(const char* const hostname, const int port)
{
    static const char* const strSchema = "http://";
    static const char* const strDelimiter = ":";
    static const int lengthSchema = strlen(strSchema);
    static const int lengthDelimiter = strlen(strDelimiter);
    static const int maxPortLength = 5;

    const int hostname_length = strlen(hostname);
    char* buf = new char[lengthSchema + hostname_length + lengthDelimiter + maxPortLength + 1];

    strcpy(buf, strSchema);
    strcpy(buf + lengthSchema, hostname);
    strcpy(buf + lengthSchema + hostname_length, strDelimiter);
    sprintf(buf + lengthSchema + hostname_length + lengthDelimiter, "%d", port);

    return buf;
}
