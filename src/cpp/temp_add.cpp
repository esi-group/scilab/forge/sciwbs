/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "gsoap/soapH.h"
#include "utilities.h"

extern "C"
{
    #include "sci_types.h"
}

static int add_double(struct soap* soap, ScilabStruct *a, ScilabStruct *b, struct ns1__addResponse *res)
{
    // TODO Check here that the matrices are of the same shape
    const int rows = MatrixUtilities::getRows(a);
    const int cols = MatrixUtilities::getCols(a);
    const int size = rows * cols;

    const bool complexA = DoubleMatrixUtilities::getComplex(a) != 0;
    const bool complexB = DoubleMatrixUtilities::getComplex(b) != 0;
    const bool complexC = complexA || complexB;

    ScilabStruct* pm = soap_malloc(soap, DoubleMatrixUtilities::getStorageSize(rows, cols, complexC));

    MatrixUtilities::setType(pm, 1);
    MatrixUtilities::setRows(pm, rows);
    MatrixUtilities::setCols(pm, cols);
    DoubleMatrixUtilities::setComplex(pm, complexC ? 1 : 0);

    double* pdReA = DoubleMatrixUtilities::getReDataPointer(a);
    double* pdReB = DoubleMatrixUtilities::getReDataPointer(b);
    double* pdReC = DoubleMatrixUtilities::getReDataPointer(pm);

    double* pdImA = DoubleMatrixUtilities::getImDataPointer(a);
    double* pdImB = DoubleMatrixUtilities::getImDataPointer(b);
    double* pdImC = DoubleMatrixUtilities::getImDataPointer(pm);

    for (int i = 0; i < size; i++)
    {
        pdReC[i] = pdReA[i] + pdReB[i];
        if (complexC)
        {
            pdImC[i] = (complexA ? pdImA[i] : 0) + (complexB ? pdImB[i] : 0);
        }
    }

    res->result = pm;

    return SOAP_OK;
}

static int add_double_sparse(struct soap* soap, ScilabStruct *a, ScilabStruct *b, struct ns1__addResponse *res)
{
    // TODO Check here that the matrices are of the same shape
    const int rows = MatrixUtilities::getRows(a);
    const int cols = MatrixUtilities::getCols(a);

    const bool complexA = DoubleMatrixUtilities::getComplex(a) != 0;
    const bool complexB = DoubleMatrixUtilities::getComplex(b) != 0;
    const bool complexC = complexA || complexB;

    const int totalA = SparseMatrixUtilities::getTotal(a);
    const int totalB = SparseMatrixUtilities::getTotal(b);

    const int storageSize = DoubleMatrixUtilities::getStorageSize(rows, cols, complexC);

    ScilabStruct* pm = soap_malloc(soap, storageSize);
    memset(pm, 0, storageSize);

    MatrixUtilities::setType(pm, sci_matrix);
    MatrixUtilities::setRows(pm, rows);
    MatrixUtilities::setCols(pm, cols);
    DoubleMatrixUtilities::setComplex(pm, complexC ? 1 : 0);

    const double* pdReA = DoubleSparseMatrixUtilities::getReDataPointer(a);
    const double* pdReB = DoubleSparseMatrixUtilities::getReDataPointer(b);
    double* pdReC = DoubleMatrixUtilities::getReDataPointer(pm);

    const double* pdImA = DoubleSparseMatrixUtilities::getImDataPointer(a);
    const double* pdImB = DoubleSparseMatrixUtilities::getImDataPointer(b);
    double* pdImC = DoubleMatrixUtilities::getImDataPointer(pm);

    const int* linesA = SparseMatrixUtilities::getLineItems(a);
    const int* linesB = SparseMatrixUtilities::getLineItems(b);

    const int* columnPositionsA = SparseMatrixUtilities::getColumnPositions(a);
    const int* columnPositionsB = SparseMatrixUtilities::getColumnPositions(b);

    int row = 0;
    int inCurrentRow = 0;
    for (int k = 0; k < totalA; k++)
    {
        for (; linesA[row] == 0; row++);

        const int column = columnPositionsA[k] - 1;

        pdReC[column * rows + row] += pdReA[k];
        if (complexA)
        {
            pdImC[column * rows + row] += pdImA[k];
        }

        inCurrentRow++;
        // find the next row with non-zero elements
        if (inCurrentRow == linesA[row])
        {
            row++;
            inCurrentRow = 0;
        }
    }

    row = 0;
    inCurrentRow = 0;
    for (int k = 0; k < totalB; k++)
    {
        for (; linesB[row] == 0; row++);

        const int column = columnPositionsB[k] - 1;

        pdReC[column * rows + row] += pdReB[k];
        if (complexB)
        {
            pdImC[column * rows + row] += pdImB[k];
        }

        inCurrentRow++;
        // find the next row with non-zero elements
        if (inCurrentRow == linesB[row])
        {
            row++;
            inCurrentRow = 0;
        }
    }

    res->result = pm;

    return SOAP_OK;
}

static int add_boolean(struct soap* soap, ScilabStruct *a, ScilabStruct *b, struct ns1__addResponse *res)
{
    // TODO Check here that the matrices are of the same shape
    const int rows = MatrixUtilities::getRows(a);
    const int cols = MatrixUtilities::getCols(a);
    const int size = rows * cols;

    ScilabStruct* pm = soap_malloc(soap, BooleanMatrixUtilities::getStorageSize(rows, cols));

    MatrixUtilities::setType(pm, sci_boolean);
    MatrixUtilities::setRows(pm, rows);
    MatrixUtilities::setCols(pm, cols);

    int* piA = BooleanMatrixUtilities::getDataPointer(a);
    int* piB = BooleanMatrixUtilities::getDataPointer(b);
    int* piC = BooleanMatrixUtilities::getDataPointer(pm);

    for (int i = 0; i < size; i++)
    {
        piC[i] = (piA[i] != 0 || piB[i] != 0) ? 1 : 0;
    }

    res->result = pm;

    return SOAP_OK;
}

static int add_boolean_sparse(struct soap* soap, ScilabStruct *a, ScilabStruct *b, struct ns1__addResponse *res)
{
    // TODO Check here that the matrices are of the same shape
    const int rows = MatrixUtilities::getRows(a);
    const int cols = MatrixUtilities::getCols(a);

    ScilabStruct* pm = soap_malloc(soap, BooleanMatrixUtilities::getStorageSize(rows, cols));

    MatrixUtilities::setType(pm, sci_boolean);
    MatrixUtilities::setRows(pm, rows);
    MatrixUtilities::setCols(pm, cols);

    int* piA = BooleanMatrixUtilities::getDataPointer(a);
    int* piB = BooleanMatrixUtilities::getDataPointer(b);
    int* piC = BooleanMatrixUtilities::getDataPointer(pm);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            piC[j * rows + i] = (SparseMatrixUtilities::isElementPresent(a, i, j) ||
                SparseMatrixUtilities::isElementPresent(b, i, j)) ? 1 : 0;
        }
    }

    res->result = pm;

    return SOAP_OK;
}

int ns1__add(struct soap* soap, ScilabStruct *a, ScilabStruct *b, struct ns1__addResponse *res)
{
    const int typeA = MatrixUtilities::getType(a);
    const int typeB = MatrixUtilities::getType(b);

    const int rowsA = MatrixUtilities::getRows(a);
    const int colsA = MatrixUtilities::getCols(a);
    const int rowsB = MatrixUtilities::getRows(b);
    const int colsB = MatrixUtilities::getCols(b);

    if (typeA != typeB)
    {
        return SOAP_USER_ERROR;
    }

    if (rowsA != rowsB || colsA != colsB)
    {
        return SOAP_USER_ERROR;
    }

    int ret = SOAP_OK;
    switch (typeA)
    {
        case sci_matrix:
            ret = add_double(soap, a, b, res);
            break;
        case sci_sparse:
            ret = add_double_sparse(soap, a, b, res);
            break;
        case sci_boolean:
            ret = add_boolean(soap, a, b, res);
            break;
        case sci_boolean_sparse:
            ret = add_boolean_sparse(soap, a, b, res);
            break;
    }

    if (ret != SOAP_OK)
    {
        return SOAP_USER_ERROR;
    }
    else
    {
        return SOAP_OK;
    }
}
