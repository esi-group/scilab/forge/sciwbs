/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"

SOAP* SOAP::_instance = 0;

SOAP::SOAP()
{

}

SOAP::~SOAP()
{

}

SOAP& SOAP::Instance()
{
    if (_instance == 0)
    {
        _instance = new SOAP;
    }
    return *_instance;
}

int SOAP::run(const char* const hostname, const int port)
{
    // calculate the next id
    int id;
    if (servers.empty())
    {
        id = 0;
    }
    else
    {
        // get the end iterator
        std::map<int, SOAPServer*>::iterator it = servers.end();

        // move to the last element
        it--;

        // get the next ID
        id = it->first + 1;
    }

    // create and run a new server
    SOAPServer* server = new SOAPServer();
    try
    {
        server->run(hostname, port);
    }
    catch (ServerStartException)
    {
        delete server;
        throw;
    }

    // if successful, put in the map
    servers[id] = server;

    return id;
}

void SOAP::terminate(const int id)
{
    // if no server is running - throw an exception
    if (servers.empty())
    {
        throw NoServerRunningException();
    }

    // find the server with the given ID
    std::map<int, SOAPServer*>::iterator it = servers.find(id);
    if (it == servers.end())
    {
        throw ServerNotFoundException(id);
    }

    // terminate the server
    it->second->terminate();

    // delete the server object
    delete it->second;

    // remove the element from the map
    servers.erase(it);
}

void SOAP::terminate()
{
    terminate(getDefaultServerId());
}

std::list<ServerEntry> SOAP::list()
{
    // create the container of the necessary type
    std::list<ServerEntry> ret;

    // fill the list with the currently running servers
    std::map<int, SOAPServer*>::iterator it;
    for (it = servers.begin(); it != servers.end(); it++)
    {
        const SOAPServer* const ps = it->second;
        ret.push_back(ServerEntry(it->first, ps->getHostName(), ps->getPort()));
    }

    return ret;
}

void SOAP::exportFunctions(std::list<std::string>& functionNames, int serverId)
{
    // check there are servers running
    if (servers.empty())
    {
        throw NoServerRunningException();
    }

    // find the server with this id
    std::map<int, SOAPServer*>::iterator found = servers.find(serverId);

    // if not found - throw an exception
    if (found == servers.end())
    {
        throw ServerNotFoundException(serverId);
    }

    // get the server object
    SOAPServer* const server = found->second;

    // export the functions one by one
    std::list<std::string>::iterator it;
    for (it = functionNames.begin(); it != functionNames.end(); it++)
    {
        server->exportFunction(*it);
    }
}

void SOAP::exportFunctions(std::list<std::string>& functionNames)
{
    exportFunctions(functionNames, getDefaultServerId());
}

void SOAP::unexportFunctions(std::list<std::string>& functionNames, int serverId)
{
    // check there are servers running
    if (servers.empty())
    {
        throw NoServerRunningException();
    }

    // find the server with this id
    std::map<int, SOAPServer*>::iterator found = servers.find(serverId);
    if (found == servers.end())
    {
        throw ServerNotFoundException(serverId);
    }

    // get the server object
    SOAPServer* const server = found->second;

    // unexport the functions one by one
    std::list<std::string>::iterator it;
    for (it = functionNames.begin(); it != functionNames.end(); it++)
    {
        server->unexportFunction(*it);
    }
}

void SOAP::unexportFunctions(std::list<std::string>& functionNames)
{
    unexportFunctions(functionNames, getDefaultServerId());
}

std::list<std::string> SOAP::listFunctions(int serverId)
{
    // find the server with this id
    std::map<int, SOAPServer*>::iterator found = servers.find(serverId);
    if (found == servers.end())
    {
        throw ServerNotFoundException(serverId);
    }

    // get the server object
    SOAPServer* const server = found->second;

    return server->getFunctions();
}

std::list<std::string> SOAP::listFunctions()
{
    return listFunctions(getDefaultServerId());
}

int SOAP::getDefaultServerId() const
{
    // if no server is running - throw an exception
    if (servers.empty())
    {
        throw NoServerRunningException();
    }

    // if more than 1 server is running - throw an exception
    if (servers.size() > 1)
    {
        throw MultipleServersRunningException();
    }

    // get the id of the only running SOAP server
    return servers.begin()->first;
}
