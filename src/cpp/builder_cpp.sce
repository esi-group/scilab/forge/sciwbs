// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

src_cpp_path = get_absolute_file_path('builder_cpp.sce');

CPPFLAGS = "-I" + src_cpp_path + " -I" + src_cpp_path + "gsoap/ " + " -I/usr/include/libcsoap-1.0" + ..
           " -I/usr/include/nanohttp-1.0" + " -I/usr/include/libxml2 " + " -lcsoap -lnanohttp";

files = ['add.cpp', 'SOAP.cpp', 'temp_add.cpp', 'SOAPServer.cpp', 'list_functions_server.cpp', ..
         'list_functions_client.cpp', 'gsoap/soapServer.cpp', 'gsoap/soapClient.cpp', ..
         'gsoap/stdsoap2.cpp', 'gsoap/soapC.cpp', 'utilities.cpp', 'invoke.cpp'];

tbx_build_src(['run', 'add'], files, 'c', src_cpp_path, '', '', CPPFLAGS);

clear tbx_build_src;
clear src_cpp_path;
clear CPPFLAGS;
clear files;
