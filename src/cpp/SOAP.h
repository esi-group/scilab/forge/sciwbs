/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_H__
#define __SOAP_H__

#include "SOAPServer.h"
#include "ServerEntry.h"

#include "soap_exceptions.h"

#include <map>
#include <list>
#include <string>

class SOAP
{
private:

    static SOAP* _instance;

    std::map<int, SOAPServer*> servers;

protected:

    SOAP();
    ~SOAP();

public:

    /**
     * Return the instance of the singleton class
     * @return the only instance of the SOAP class
     */
    static SOAP& Instance();

    /**
     * Runs a SOAP server on given hostname and port
     * @param[in] hostname the hostname to run the SOAP server on
     * @param[in] port the port to run the SOAP server on
     * @return a positive value representing the ID of the server started if
     * completed successfully, a negative value otherwise
     */
    int run(const char* const hostname, const int port);

    /**
     * Terminates the SOAP server with a given ID
     * @param[in] id the identifier of the server to be terminated
     */
    void terminate(const int id);

    /**
     * Terminates the only running SOAP server and returns an error 
     * if no SOAP server is running or more than 1 SOAP servers are running
     * @param[in] id the identifier of the server to be terminated
     */
    void terminate();

    /**
     * Lists all running SOAP servers
     * @return list containing entries with running servers information
     */
    std::list<ServerEntry> list();

    /**
     * Exports the specified functions to the SOAP server with a given ID
     * @param[in] functions the list of exported functions
     * @param[in] serverId the identifier of the server
     */
    void exportFunctions(std::list<std::string>& functionNames, int serverId);

    /**
     * Exports the specified functions to the only running SOAP server
     * and returns an error if no SOAP server is running or more than 1
     * SOAP servers are running
     * @param[in] functions the list of exported functions
     */
    void exportFunctions(std::list<std::string>& functionNames);

    /**
     * Unexports the specified functions from the SOAP server with a given ID
     * @param[in] functions the list of unexported functions
     * @param[in] serverId the identifier of the server
     */
    void unexportFunctions(std::list<std::string>& functionNames, int serverId);

    /**
     * Unexports the specified functions from the only running SOAP server
     * and returns an error if no SOAP server is running or more than 1
     * SOAP servers are running
     * @param[in] functions the list of unexported functions
     */
    void unexportFunctions(std::list<std::string>& functionNames);

    /**
     * Lists all functions exported to the server with a given ID
     * @param[in] serverId the identifier of the server
     * @return a list of functions exported to the server
     */
    std::list<std::string> listFunctions(int serverId);

    /**
     * Lists all functions exported to the default server
     * @return a list of functions exported to the default server
     */
    std::list<std::string> listFunctions();

private:

     /**
      * Returns the identifier of the only running SOAP server
      * @return the identifier of the only running SOAP server
      */
    int getDefaultServerId() const;
};

#endif /* __SOAP_H__ */
