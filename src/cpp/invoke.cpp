/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "invoke.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <libcsoap/soap-client.h>
#include <nanohttp/nanohttp-logging.h>

#include "utilities.h"

/*herror_t soap_ctx_new_with_body(const char * body, SoapCtx ** out);
herror_t soap_env_new_with_body(const char * body, SoapEnv ** out);

herror_t soap_ctx_new_with_body(xmlNode * body, SoapCtx ** out);
herror_t soap_env_new_with_body(xmlNode * body, SoapEnv ** out);*/

int invoke(const char* const endpoint, const char* const action, xmlDoc* const request, xmlDoc** const response)
{
    SoapCtx *req = NULL;
    SoapCtx *res = NULL;
    herror_t err;

    if ((err = soap_client_init_args(0, NULL)) != H_OK)
    {
        printf("%s():%s [%d]\n", herror_func(err), herror_message(err), herror_code(err));
        herror_release(err);
        return -1;
    }

    // Envelope creation
    SoapEnv *env;
    err = soap_env_new_from_doc(request, &env);
    if (err != H_OK)
        return -1;
    req = soap_ctx_new(env);

    // Invocation
    if ((err = soap_client_invoke(req, &res, endpoint, action)) != H_OK)
    {
        printf("[%d] %s(): %s\n", herror_code(err), herror_func(err), herror_message(err));
        herror_release(err);
        soap_ctx_free(req);
        soap_client_destroy();
        return -1;
    }

    if(!(*response = xmlCopyDoc(res->env->root->doc, 1)))
    {
        soap_ctx_free(res);
        soap_ctx_free(req);
        soap_client_destroy();
        return -1;
    }

    // Request cleanup
    soap_ctx_free(res);
    soap_ctx_free(req);

    // Client cleanup
    soap_client_destroy();

    return 0;
}
